const fs = require('fs');
const PO = require('pofile');
const xlsx = require('xlsx');

// Get files
const allFiles = fs.readdirSync('./to_po');
// Filter xlsx files
const xlsxFiles = allFiles.filter(file => file.endsWith('.xlsx'));


// Find locale name in fields
function findLocale(rows) {
    let fields =  ['Message context', 'Speaker', 'Message id', 'Opponent', 'Source comment' , 'Act', 'Location', 'Node'];
    if(rows[1]) {
        return Object.keys(rows[1]).find(k => !fields.includes(k));
    }
    return Object.keys(rows[0]).find(k => !fields.includes(k));
}

for (let file of xlsxFiles) {

    console.info('Start сonversion: ' + file);

    // Workbook
    let wb = xlsx.readFile('./to_po/' + file)
    // Worksheet
    let ws = wb.Sheets[wb.SheetNames[0]];

    // Convert sheet to json
    let rows = xlsx.utils.sheet_to_json(ws);

    // New po file
    let po = new PO();

    let localeFieldName = findLocale(rows);

    console.info('Find locale: ' + localeFieldName);

    for (row of rows) {
        let item = new PO.Item();

        item.msgid = String(row['Message id']);
        item.msgctxt = String(row['Message context']);
        item.msgstr = String(row[localeFieldName] ? row[localeFieldName] : '');
        item.extractedComments = [row['Source comment']];

        po.items.push(item);
    }

    po.save('./to_po/' + file.slice(0,-5) + '.po', function(err){
        console.info('Conversion complete: ' + file);  
    });
}