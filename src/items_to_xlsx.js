const fs = require('fs');
const PO = require('pofile');
const xl = require('excel4node');

// Get files
const allFiles = fs.readdirSync('./to_xlsx');
// Filter po files
const poFiles = allFiles.filter(file => file.endsWith('.po'));

for(let file of poFiles) {

    console.info('Start сonversion: ' + file);

    PO.load('./to_xlsx/' + file, function(err, po) {

        // Create workbox
        let wb = new xl.Workbook();
        // Create workheet
        let ws = wb.addWorksheet('Translations');
    
        // Set columns headers
        ws.cell(1,1).string('Message context');
        ws.cell(1,2).string('Message id');
        ws.cell(1,3).string('Item name');
        ws.cell(1,4).string('Field name');
        ws.cell(1,5).string(po.headers.Language);
        ws.cell(1,6).string('Source comment');
    
        let row = 2;
    
        for (let item of po.items) {
            let refs = item.references[0].split('/');

            let dtn1, dtn2, itemName, fieldName;

            if(refs[5]) {
                [dtn1, dtn2, itemName, fieldName] = refs[5].split('.');
            }
            else {
                [dtn1, dtn2, itemName, fieldName] = refs[4].split('.');
            }
            
            
            ws.cell(row, 1).string(item.msgctxt);
            ws.cell(row, 2).string(item.msgid);
            ws.cell(row, 3).string(itemName);
            ws.cell(row, 4).string(fieldName.split('_')[0]);
            ws.cell(row, 5).string(item.msgstr[0]);
            ws.cell(row, 6).string(item.extractedComments);
            //console.log(item);

            row++;
        }
    
        // Save xlsx file
        wb.write('./to_xlsx/' + file.slice(0,-3) + '_' + po.headers.Language + '.xlsx');

    });

    console.info('Conversion complete: ' + file);
}