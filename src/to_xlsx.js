const fs = require('fs');
const PO = require('pofile');
const xl = require('excel4node');

// Get files
const allFiles = fs.readdirSync('./to_xlsx');
// Filter po files
const poFiles = allFiles.filter(file => file.endsWith('.po'));

for(let file of poFiles) {

    console.info('Start сonversion: ' + file);

    PO.load('./to_xlsx/' + file, function(err, po) {

        //console.log(po)
    
        // Create workbox
        let wb = new xl.Workbook();

        // Create workheet
        let ws = wb.addWorksheet('Translations');
    
        // Set columns headers
        ws.cell(1,1).string('Message context');
        ws.cell(1,2).string('Speaker');
        ws.cell(1,3).string('Message id');
        ws.cell(1,4).string(po.headers.Language);
        ws.cell(1,5).string('Opponent');
        ws.cell(1,6).string('Source comment');
        ws.cell(1,7).string('Act');
        ws.cell(1,8).string('Location');
        ws.cell(1,9).string('Node');
    
        let row = 2;
    
        for (let item of po.items) {
            let refs = item.references[0].split('/');
            let additionalRefs = refs[refs.length - 1].split('.');
            let nodeRefs = additionalRefs[1].split(':');
            let nodeName = nodeRefs[1];

            let nodeNubTmp = nodeName.split('_');

            // Get end element
            let nodeNumber = nodeNubTmp[nodeNubTmp.length - 1];
    
            ws.cell(row, 1).string(item.msgctxt);
    
            if(additionalRefs[2] === 'Text') {
                ws.cell(row, 2).string(additionalRefs[0]);
            }
            else {
                ws.cell(row, 2).string('Player ' + additionalRefs[2]);
            }
            // Original text id
            ws.cell(row, 3).string(item.msgid);
            // Translated text
            ws.cell(row, 4).string(item.msgstr[0]);
            // Opponent
            ws.cell(row, 5).string(nodeRefs[0]);
            // Source comment
            ws.cell(row, 6).string(item.extractedComments);
            // Act
            ws.cell(row, 7).string(refs[4]);
            // Location
            ws.cell(row, 8).string(refs[5]);
            // Node
            ws.cell(row, 9).number(Number(nodeNumber));
            
            row++;
        }
    
        // Save xlsx file
        wb.write('./to_xlsx/' + file.slice(0,-3) + '_' + po.headers.Language + '.xlsx');
    });

    console.info('Conversion complete: ' + file);
}