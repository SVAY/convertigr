const fs = require('fs');
const path = require('path');

const rootFolder = './rename';

const folders = fs.readdirSync(rootFolder);

for (let folder of folders) {

    let folderPath = `${rootFolder}/${folder}`;

    let folderFiles = fs.readdirSync(folderPath);

    if(!folderFiles.length) {
        console.warn('WARNING: Empty folder: ' + folder);
    }

    for (let file of folderFiles) {
        let fileExtension = path.extname(file);
        let fileWithoutExt = file.split('.').slice(0, -1).join('.');

        let needName = `${folderPath}/${fileWithoutExt}_${folder}${fileExtension}`;

        fs.rename(`${folderPath}/${file}`, needName, () => {
            console.log('Renamed to: ' + needName);
        });
    }
}

